 Original artwork and story by
David Revoy

 Artwork Processing
Anastasia Mayzhegisheva
Anastasia Nelyubina
Dmitry Abyshev
Sofya Vyborova
Natalya Pesterova
Olga Filippova
Alina Tabyshkina
Anna Aleinikova
Alexandr Borisov

 Directed by
Anastasia Mayzhegisheva

 Animatic
Anastasia Mayzhegisheva

 Animation
Anastasia Mayzhegisheva
Anastasia Nelyubina
Anna Aleinikova
Sofya Vyborova

 Editing
Anastasia Mayzhegisheva
Stas Kholodilin

 Cast (English)
Pepper - Samantha Faye Ignacio
Carrot - Alexander Filchenko
Narrator - Duffy Weber

 Cast (Russian)
Пеппер - Елена Симанович
Кэррот - Александр Фильченко
Голос за кадром - Александр Фильченко

 Translation (English)
Amireeti
Stas Kholodilin

 Translation (Russian)
Стас Холодилин
Денис Присухин


 Music
"Toys Revolution" by Gregoire Lourme
"Waltz in Low Light" by Nat Keefe & Hot Buttered Rum
"Rainy Day Games" by The Green Orbs
"Mourning Dove" by Zachariah Hickman
"Mundo Canibal" by The Freak Fandango Orchestra

 Sound Design
Stas Kholodilin

 Produced by
Konstantin Dmitriev


  Based on
the webcomic "Pepper & Carrot" by David Revoy
https://www.peppercarrot.com/
  and
Universe of Hereva created by David Revoy with contributions by Craig Maloney.
Corrections by Willem Sonke, Moini, Hali, CGand and Alex Gryson.
 
 
This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 license.
https://creativecommons.org/licenses/by-sa/4.0/


  Community contributions

Your Name
- Contribution 1 short description
- Contribution 2 short description
- ...
