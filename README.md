# Source files of **Pepper & Carrot Motion Comic - Episode 4**.

IMPORTANT: This repository uses Git LFS (https://git-lfs.github.com/). Please make sure to install it before you clone.

To build sources of this animation you will need following software installed:

- Blender 2.83.13
- Synfig 1.4.2
- RenderChan (https://morevnaproject.org/renderchan/)

NOTE: All those applications are free software, so you can download and use them for free.

If you wish to edit artwork, then you need to install Krita (https://krita.org/).

Rendering files:

```
renderchan ~/peppercarrot-ep04/project-en.blend
```

When rendering is completed you will find resulting file in "render" subdirectory - `~/peppercarrot-ep04/render/project-en.blend.avi`.

NOTE: It is assumed you unpacked project sources into “~/peppercarrot-ep04/” directory.

For more details about working with sources of this animation see detailed instructions for other episode - https://morevnaproject.org/2017/08/12/work-sources-pepper-carrot-episode-6/

If you plan to contribute into this repository, please consider to read important instructions here - https://gitlab.com/OpenSourceAnimation/pepper-and-carrot-ep04/-/blob/master/CONTRIBUTING.md
